/*
 * MIT License
 *
 * Copyright (c) 2021. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package `in`.technowolf.neet

/**
 * A simple method that will return true if internet is connected, false if not.
 *
 * @return [Boolean]
 *
 */
fun isInternetConnected() = NeetEngine.isConnected()

/**
 * Returns livedata to observe. Livedata will emit boolean values when observed.
 * If internet is connected true will be emitted, false if not.
 *
 * Emitting frequency will depend on given [NeetObserver] interval.
 *
 * @return [LiveData] with boolean
 *
 */
fun isInternetConnectedLiveData() = NeetEngine.isConnectedLiveData()

/**
 * Detaches the observer when called.
 *
 */
fun detachNetworkObserver() = NeetEngine.detachObserver()

/**
 * Extension DSL that will execute lambda when internet is available.
 *
 * @param executable Lambda block to execute with internet is available.
 *
 */
fun doWhenInternetAvailable(executable: () -> Unit) {
    if (isInternetConnected()) executable()
}

/**
 * Extension DSL that will execute lambda when internet is available and another lambda when
 * internet is unavailable.
 *
 * @param successBlock Lambda block to execute with internet is available.
 * @param failureBlock Lambda block to execute with internet **not** available.
 *
 */
fun doWithInternetConnection(successBlock: () -> Unit, failureBlock: () -> Unit) {
    if (isInternetConnected()) successBlock()
    else failureBlock()
}
